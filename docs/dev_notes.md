Insightful help given by Jeffrey A. Stanley, PhD
- Use LCModel's bin2raw to generate the RAW file
- This function is still vendor specific, so choose the correct one.

The RAW file appears to be a headerless representation of the data with  
the SEQPAR and NMID namelists at the beginning.

- The example control file (init.control) tries to define the fields a little more simply than the LC manual does. However, clarifications from the scientists often point back to the manual. Hence, the pdf is in this same docs folder.

- Gaurav provided time estimates to model the data on a voxel-level basis for human participants. The timeout estimates are based off his estimates.

- The gear is now set to most often build the control file from the init.control file and UI configuration tab inputs. Advanced users can still override that path by supplying a control file. All control files go through a validation within the gear to attempt to identify issues that Fortran will have with formatting.

- memalloc() errors with testrun or a real run probably means that the given key is not the open source key. Why they can't just say that it is a key error, I don't know.

- The RAW file has to be given to the gear. The GUI version of LCModel creates it on the fly from the basis set and some control parameters, but the batch mode needs the the file to be created already.