# Release notes

## 0.0.1_6.3
Initial release of LCModel as a Flywheel gear.
__Documentation__:

* Add `docs`folder
* Add `docs/release_notes.md`

Notes: Categories used in release notes should match one of the following:

* Fixes
* Enhancements
* Documentation
* Maintenance
