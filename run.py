#!/usr/bin/env python

"""The run script"""
import logging
import sys

import flywheel
from flywheel_gear_toolkit import GearToolkitContext
from flywheel_gear_toolkit.utils import datatypes
from flywheel_gear_toolkit.utils.curator import get_curator

from fw_gear_lcmodel.fw_utils import zip_it
from fw_gear_lcmodel.main import run

log = logging.getLogger(__name__)


def main(gear_context: GearToolkitContext) -> None:  # pragma: no cover
    """Parses config and run"""

    # Pass the args, kwargs to fw_gear_lcmodel.main.run function to execute
    # the main functionality of the gear.
    e_code = run(gear_context)

    if e_code == 0:
        zip_it(gear_context)

    # Exit the python script (and thus the container) with the exit
    # code returned by example_gear.main.run function.
    sys.exit(e_code)


# Only execute if file is run as main, not when imported by another module
if __name__ == "__main__":  # pragma: no cover
    # Get access to gear config, inputs, and sdk client if enabled.
    with GearToolkitContext() as gear_context:
        # Initialize logging, set logging level based on `debug` configuration
        # key in gear config.
        gear_context.init_logging()

        # Pass the gear context into main function defined above.
        main(gear_context)
