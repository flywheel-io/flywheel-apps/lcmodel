FROM ubuntu:20.04 as base
LABEL MAINTAINER Flywheel <support@flywheel.io>


# LC model is now freely available. LC Model homepage is
# http://lcmodel.ca/lcmodel.shtml
# No version information is noted.
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y --no-install-recommends \
    xvfb \
    xfonts-100dpi \
    xfonts-75dpi \
    xfonts-cyrillic \
    git \
    python3-pip \
    python3 \
    # For lcmgui
    python3-tk \
    tix \
    curl \
    # Modelsim dependencies
    libxft2 && \
    apt install -y --no-install-recommends \
    libxext6 && \
    # Keep the Docker image as small as possible
    rm -rf /var/lib/apt/lists/* && \
   # Provide output file name so the commandline output doesn't go crazy
    curl -sSL --output lcm-64.tar http://lcmodel.ca/pub/LCModel/programs/lcm-64.tar && \
    mkdir /opt/lc && \
    tar -xf lcm-64.tar -C /opt/lc &&\
    cd /opt/lc && \
    ./install-lcmodel && \
    # Install one-time license key
    echo "key = 210387309" > ~/.lcmodel/license

# Install poetry based on their preferred method. pip install is finnicky.
# Designate the install location, so that you can find it in Docker.
ENV PYTHONUNBUFFERED=1 \
    POETRY_VERSION=1.1.6 \
    # make poetry install to this location
    POETRY_HOME="/opt/poetry" \
    # do not ask any interactive questions
    POETRY_NO_INTERACTION=1
RUN pip3 install update pip && \
    ln -s /usr/bin/python3 /usr/bin/python
ENV PATH="$POETRY_HOME/bin:/usr/bin/python3:$PATH"

# xvfb x-windows frame buffer setting
ENV DISPLAY =:1.0

# get-poetry respects ENV
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

# Installing main dependencies
COPY pyproject.toml poetry.lock $FLYWHEEL/
RUN poetry install --no-dev

# Installing the current project (most likely to change, above layer can be cached)
# Note: poetry requires a README.md to install the current project
COPY run.py manifest.json README.md $FLYWHEEL/
COPY fw_gear_lcmodel $FLYWHEEL/fw_gear_lcmodel

# Must include the following override to be able to test the installation.
# memalloc() error on the testrun is a bunny trail. Parker figured out that the key in the testfile is bogus. If you put the correct key in there, the test runs on his system.
COPY test.control /root/.lcmodel/test/control/test.control

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py &&\
    chmod a+x $FLYWHEEL/fw_gear_lcmodel/run_lcmodel

ENTRYPOINT ["poetry","run","python","/flywheel/v0/run.py"]
