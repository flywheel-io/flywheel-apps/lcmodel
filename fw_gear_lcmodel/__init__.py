"""The fw_gear_lcmodel package."""
from importlib.metadata import version

try:
    __version__ = version(__package__)
except:  # pragma: no cover
    pass
