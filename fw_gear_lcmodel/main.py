"""Main module."""
import functools
import logging
import operator
import os.path as op
import re
import subprocess as sp
import sys
import time
from collections import namedtuple
from glob import glob
from os import stat
from pathlib import Path
from timeit import default_timer as timer

from flywheel_gear_toolkit.interfaces import command_line

from fw_gear_lcmodel import fw_utils, parser, set_control_file

log = logging.getLogger(__name__)


def run(gear_context):
    """
    The LC Model gear:
        parses input from configuration files
        creates the RAW file(s) for LCModel
        looks for a control file, if supplied, or creates one from the config.json
        options selected
        and runs LCModel to capture metabolite spectra.
    Args
        gear_context (GearToolkit context): class filled with information about the gear,
        study, and configuration options.
    Returns
        e_code: error code; 0 for clean run, 1 for an early system exit.
    """
    debug, input_files, gear_args = parser.parse_config(gear_context)

    # Must have data converted per LCModel's vendor-specifc bin2raw method
    check_RAW_file(gear_context)

    if "control_file" in input_files.keys():
        log.info("Loading user-supplied control file.")
        user_file = gear_context.get_input_path("control_file")
        fw_utils.check_cntl_file(gear_context, user_file)
    else:
        log.info("Creating the log file from selected options.")
        fw_utils.load_init_control_file(gear_context)

    slices = determine_slices_to_process(gear_context)
    voxel_size = get_voxel_size(gear_context)
    # Calculate the estimated time to analyze the slab based on 40 minutes for
    # 1mm^3 resolution. e.g., 50 min/4mm => ~10 minutes.
    num_of_voxels = get_num_of_voxels(gear_context)
    log.info(
        f"A full slice can take between 10-40 minutes. Overall projected time is {len(slices)} slice(s) times {round(50/(voxel_size)*num_of_voxels, 2)} => {round(len(slices) * (50/voxel_size*num_of_voxels),2)} minutes."
    )

    for slc in slices:
        fw_utils.edit_slice_number(gear_context, slc)
        log.debug(f"Analyzing slice number {slc}")
        result = run_lcmodel(gear_context, slc)
    return result


def run_lcmodel(gear_context, slc):
    """
    Build and run the LCModel command. Assumes that the control file, raw data, and basis set
    have been checked and are in the locations indicated in the control file.
    Args
        gear_context (GearToolkitContext): gear information
    Returns
        result.returncode (int): Status of gear run. Zero on success.
    """

    try:
        cntl_file_path = op.join(
            gear_context.output_dir, "slice" + str(slc) + ".control"
        )
        if gear_context.config.get("debug"):
            with open(cntl_file_path, "r") as f:
                check = f.read()

        start_time = timer()
        result = sp.run(
            ["/flywheel/v0/fw_gear_lcmodel/run_lcmodel", cntl_file_path],
            stdout=sp.PIPE,
            stderr=sp.PIPE,
        )
        end_time = timer()
        if end_time - start_time < 1:
            if result.stderr:
                # LCModel had a problem and reported it through bash
                log.error(result.stdout.decode("utf-8"))
                log.error(result.stderr.decode("utf-8"))
                result.returncode = 1
            elif stat("error.txt").st_size > 0:
                # The error was captured in error.txt, presumably b/c there was
                # no LCModel output
                with open("error.txt") as f:
                    log.error(f.read())
                result.returncode = 1
            else:
                # Catch-all. No error reporting that is useful, but the user
                # should know that the model did not execute.
                log.error(
                    f"LCModel exited too quickly.\n There is likely an issue with the "
                    f"data and requested dimensions.\n Otherwise, LCModel would've "
                    f"reported the error number to cross-reference in the manual."
                )
                result.returncode = 1
        elif result.stdout:
            log.debug(result.stdout.decode("utf-8"))
            result.returncode = 0
        else:
            log.error(
                "There is an error internal to the LCModel command: {result.stderr.decode('utf-8')}"
            )
    except Exception as e:
        log.error(repr(e))
        r = namedtuple("result", "returncode")
        result = r(returncode=1)
    return result.returncode


def check_RAW_file(gear_context):
    """
    bin2raw from LCModel is intended to generate the necessary .RAW file. The script
    is written specific to each vendor, but each version is stored within ~/.lcmodel/${vendor}
    for easy implementation.
    Args
        gear_context (GearToolkit Context): gear info, specifically for vendor name and input file
    Returns
        gear_context: modified to include a "RAW file location"
    """
    ext = Path(gear_context.get_input_path("RAW")).suffix
    if ext.lower() in [".dcm", ".ima"]:
        log.error("Must input RAW file. (Run bin2raw output first)")
        sys.exit(1)
        # Many cases may already have undergone the process to create the RAW file, but
        # this clause will ensure that everyone has the RAW file that LCModel requires.
    else:
        log.info("RAW file was provided as input")
        # Possible TODO Check the format of the RAW file? i.e., does it have the SEQPAR
        # and NMID at the top?


def determine_slices_to_process(gear_context):
    """Parse the user input regarding which slices to analyze at this time.
    Args
        gear_context (GearToolkitContext)
    Returns
        slices (list): parsed, numerical list of slices
    """
    # Search for image-based slice definition
    if "slices_to_process" in gear_context.config.keys():
        raw_input = gear_context.config["slices_to_process"]
    else:
        rda_dict = set_control_file.skim_rda_hdr(
            gear_context.get_input_path("rda_file")
        )
        slice_entry = rda_dict["NumberOf3DParts"]
        end = int(slice_entry.split(":")[-1].strip())
        return range(1, end + 1)  # 1-indexed...

    # Check for user provided input in control file
    if "control_file" in gear_context.config_json["inputs"].keys():
        cntl_file_path = gear_context.get_input_path("control_file")
        with open(cntl_file_path, "r") as cf:
            # Strip all the possible interferring characters, so only a numerical
            # value is found and "islice" doesn't need to accommodate special chars.
            raw_input = [
                line.split("=")[-1].strip(" \"'\t\r\n")
                for line in cf.readlines()
                if "islice" in line
            ][0]

    # Determine the type of raw_input and format for gear
    # Check single slice
    if len(raw_input) == 1:
        return [int(raw_input)]

    # Check ranges
    if any(sym in ("-", ":") for sym in raw_input):
        # Infer the user intended for a range
        start = re.split("[-:]", raw_input)[0]
        end = re.split("[-:]", raw_input)[-1]
        return range(int(start), int(end) + 1, 1)
    else:
        # Check lists
        slices = re.split(r"[\s,.]", raw_input)
        # Filter extra, empty elements
        return [s for s in slices if s]


def get_voxel_size(gear_context):
    """Read the voxel_size given by the rda file, so that an accurate estimate
    of time and timeout can be calculated.
    Args
        gear_context (GeartoolkitContext): gear info, chiefly including context.config with all the config.json values.
    Returns
        voxel_size (int): volume, really, not voxelsize, but c'est la vie. This quantity is
                        correct for subsequent methods and is consistent with other names.
    """
    rda_dict = set_control_file.skim_rda_hdr(gear_context.get_input_path("rda_file"))
    thickness = rda_dict["SliceThickness"]
    spacing = [float(v) for k, v in rda_dict.items() if "PixelSpacing" in k]
    voxel_matrix = (
        *spacing,
        thickness,
    )  # as defined by Suspect; keep for future processing with SUSPECT + save_raw()
    voxel_size = functools.reduce(operator.mul, spacing, 1)
    return voxel_size


def get_num_of_voxels(gear_context):
    """Check the control file to determine how many voxels are being analyzed in every slice.
    Args
        gear_context (GeartoolkitContext): gear info, chiefly including context.config with all the config.json values.
    Returns
        num_of_voxels (int) = maximum index of the matrix
    """
    cntl_file_path = glob(op.join(gear_context.output_dir, "*.control"))[0]
    cntl_dict = {}
    with open(cntl_file_path, "r") as f:
        for line in f.readlines():
            try:
                k, v = line.split("=")
                cntl_dict[k.strip()] = v.strip()
            except Exception as e:
                # Some of the lines are not keys (e.g., "$LCMODL")
                pass
    x = int(cntl_dict["irowen"]) - int(cntl_dict["irowst"])
    y = int(cntl_dict["icolen"]) - int(cntl_dict["icolst"])
    return x * y
