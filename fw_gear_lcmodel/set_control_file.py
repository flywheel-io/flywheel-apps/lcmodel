# Essentially an importable dictionary
# These defaults are loaded, if the user did not specify a voxel or slice in the UI
# The idea is to start at the first voxel, first slice
# The ppm range should give most of the interesting part of the spectra. Power Users should
# supply a control file.


def define_defaults():
    return {
        "hzpppm": "MRFrequency",
        "echot": "TE",
        "deltat": "DwellTime",
        "ndslic": "NumberOf3DParts",
        "ndrows": "NumberOfRows",
        "ndcols": "NumberOfColumns",
        "ppmst": 4.0,
        "ppmend": 0.2,
        "islice": 1,
        "irowst": 1,
        "irowen": "NumberOfRows",
        "icolst": 1,
        "icolen": "NumberOfColumns",
        "nomit": 0,
        "filcsv": "/flywheel/v0/output/slice_?.csv",
        "savdir": "/flywheel/v0/output",
    }


def skim_rda_hdr(rda_filepath):
    """
    Search the header of the rda file for the values that the control file will require.
    Args
        rda_file (path): path to the converted file that has header information to parse
    Returns
        rda_data (dict)
    """
    with open(rda_filepath, "r", encoding="unicode_escape") as f:
        # Speed up the process/lower memory by only getting the first 200 lines
        contents = [next(f) for x in range(85)]

    rda_dict = {}
    for line in contents:
        if line.count(":") == 1:
            k, v = line.split(":")
            rda_dict[k.strip()] = v.strip()
    return rda_dict
