"""Parser module to parse gear config.json."""
import logging
import os.path as op
import re
import sys
from collections import OrderedDict
from math import sqrt
from typing import Tuple

import suspect
from flywheel_gear_toolkit import GearToolkitContext
from flywheel_gear_toolkit.utils.manifest import Manifest

from fw_gear_lcmodel import fw_utils, set_control_file

log = logging.getLogger(__name__)
manifest_json = Manifest(op.join(op.dirname(op.dirname(__file__)), "manifest.json"))


def parse_config(
    gear_context: GearToolkitContext,
) -> Tuple[str, str]:
    """Harvest inputs from the user and translate into parsed arguments for run.py

    Returns
        debug (str): logging level
        input_filepaths (list): file paths for files to be passed to {{gear}}
        gear_args (dict): options to be built into the {{gear}} command

    """

    debug = gear_context.config.get("debug")
    input_files = gather_input_files(gear_context)
    gear_args = parse_commands(gear_context)

    return debug, input_files, gear_args


def gather_input_files(gear_context):
    """
    Parse the "inputs" field from the config.json
    Args
        gear_context (GearToolkitContext): gear information
    Returns
        input_files (dict): commandline arg and filepath pairs
    """
    filepaths = {}
    filetypes = gear_context.config_json["inputs"].keys()
    for ft in filetypes:
        if ft not in ["api-key", "key"]:
            filepaths[ft] = gear_context.get_input_path(ft)
    return filepaths


def parse_commands(gear_context):
    """
    Elements from the config.json are entered here to be filtered for LCModel related commands and ordering.
    Items in the blacklist are either Flywheel settings to control other method behaviors or LCModel items that
    must adhere to positional order in the final command.
    Args
        gear_context (GeartoolkitContext): gear info, chiefly including context.config with all the config.json values.
    Returns
        args (dict): parameter settings that can be passed to build_command_list
        gear_context is updated.
    """
    # Specify context config "do_not_include" to avoid incorporating commands
    # into command line arguments
    do_not_include = ["debug"]

    gear_args = OrderedDict()
    for arg, val in gear_context.config.items():
        if (arg not in do_not_include) and (val not in ("False", "None")):
            gear_args[arg] = val

    if "nomit" in gear_context.config.keys():
        if "chomit" not in gear_context.config.keys():
            log.error(
                f"{gear_context.config['nomit']} metabolites are to be excluded,\n"
                f"but no metabolites are identified. Add to 'chomit' option or the "
                f"submitted control file."
            )
            sys.exit(1)

    if "chomit" in gear_context.config.keys():
        # Case if person has not populated nomit, but there are metabolites listed. Much easier
        # condition to catch and handle internally, rather than above where metabolites need to
        # be specified by the expert.
        format_chomit(gear_context)
        if "nomit" not in gear_context.config.keys() or gear_context.config[
            "nomit"
        ] != len(gear_context.config["chomit"]):
            log.info("Correcting number of omitted metabolites.")
            gear_context.config["nomit"] = len(gear_context.config["chomit"])

    return gear_args


def format_chomit(gear_context):
    """Attempt to predict odd ways that people may add metabolites to exclude.
    Split the field based on the odd delimiters and overwrite the entry.
    Args
        gear_context (GeartoolkitContext): gear info, chiefly including context.config with all the config.json values.
    Returns
        gear_context is updated
    """
    val = gear_context.config["chomit"]
    if not isinstance(val, list):
        if any(sym in (",", " ", ";") for sym in val):
            val = re.split(r"[\s,;]", val)
            # Clean out the empty strs
            val = [v for v in val if v]
        else:
            # Single metabolite
            val = [val]
    gear_context.config["chomit"] = val


def parse_init_control_file(gear_context, contents):
    """
    Build up a dictionary of values with defaults from input, rda hdr, and init.control file sources.
    This dictionary can be updated by other functions to include user-provided options
    Args
        gear_context (GeartoolkitContext): gear info, chiefly including context.config with all the config.json values.
    Returns
        contents_dict (dict): Pairs contain the control entry and parameter value
    """
    # Load the .rda header once.
    rda_hdr = set_control_file.skim_rda_hdr(gear_context.get_input_path("rda_file"))
    rda_data = suspect.io.load_rda(gear_context.get_input_path("rda_file"))
    # Load the defaults dictionary once.
    defaults = set_control_file.define_defaults()
    contents_dict = OrderedDict()
    for l, line in enumerate(contents):
        key, val = line.split("=")
        key = key.strip()
        val = val.strip()
        # Filepath fields
        if key in ["srcraw", "filraw", "filbas", "filcsv", "savdir", "filps", "filtab"]:
            val = fw_utils.add_reqd_files(gear_context, key)
        # Configurable options from the UI
        elif key in [
            "ppmst",
            "ppmend",
            "islice",
            "irowst",
            "irowen",
            "icolst",
            "icolen",
            "nomit",
        ]:
            if key in gear_context.config.keys():
                val = gear_context.config[key]
            else:
                val = defaults[key]
                if key == "icolen":
                    # val = int(rda_hdr[hdr_key])
                    val = rda_data.shape[0]
                elif key == "irowen":
                    val = rda_data.shape[1]
        else:
            # The remainder should basically always be set from the rda header.
            # Power users will have their own control files to override the necessity of
            # building this.
            try:
                hdr_key = defaults[key]  # Translation mechanism
                if key == "deltat":
                    # Convert to ms
                    val = int(rda_hdr[hdr_key]) / 1000000
                # elif key in ["ndrows", "ndcols", "ndslic"]:
                # val = int(rda_hdr[hdr_key])
                elif key == "ndrows":
                    # Rows are not an issue for running the model
                    val = int(rda_data.shape[1])
                elif key == "ndcols":
                    val = int(rda_data.shape[0])
                elif key == "ndslic":
                    val = int(rda_data.shape[2])
                else:
                    val = rda_hdr[hdr_key]
            except KeyError:
                # For a few entries, it is acceptable to retain the value from the init.control file
                # If there was not an original value and it wasn't populated elsewhere, then there
                # is a problem.
                if key == "nunfil":
                    val = rda_data.np
                if not val:
                    log.info(f"Did not find a value for {key}")
        contents_dict[key] = val
    return contents_dict


def format_values(key, val):
    """
    In order to add the correctly formatted line to the control file,
    the datatype needs to be assessed and attempted to be coerced into
    appropriate numeric types. Errant single quotes will mess up LCModel.
    Args
        key (str): name of the entry for the control file line.
        val (str, most likely): the associated value that needs to be
            checked and potentially coerced.
    Returns
        properly formatted line to add to the control file
    """
    if isinstance(val, list):
        # chomit is likely the only field
        msg = []
        log.debug(f"Implementing array of entries for {key}.")
        for v, met in enumerate(val):
            msg.append(f" {key}({v+1})= '{met}'\n")
        return f"".join(msg)

    if isinstance(val, str):
        try:
            # Attempt int first, since it won't include floats automatically
            val = int(val)
            return " {}={}\n".format(key, val)
        except:
            try:
                # Try float
                val = float(val)
                return " {}={}\n".format(key, val)
            except:
                # Resort to str
                return " {}='{}'\n".format(key, val)
    else:
        # Already float or int
        return " {}={}\n".format(key, val)


def add_config_opts(gear_context):
    """Convert the config options from the UI into dictionary entries to update default values.
    Args
        gear_context
    Returns
        ui_dict (dict): k,v pairs that were provided by the user and are formatted to match LCModel
        entries. Intended to update the contents_dict directly.
    """
    ui_dict = OrderedDict()
    # Some of the config options are for the $SEQPAR or $NMID namelists that go into the first
    # chunk of the RAW file (pp.35-36 of the LC Manual)
    excl_opts = [
        "echot",
        "seq",
        "fmtdat",
        "volume",
        "tramp",
        "Bruker",
        "vendor",
        "debug",
        "slices_to_process",
        "dry_run",
    ]
    ui_dict = {k: v for (k, v) in gear_context.config.items() if not k in excl_opts}
    return ui_dict
