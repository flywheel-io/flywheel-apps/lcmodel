import logging
import os.path as op
import re
import shutil
import sys
from glob import glob
from os import makedirs, remove
from pathlib import Path

from flywheel import ApiException
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_lcmodel import main, parser

log = logging.getLogger(__name__)


def load_init_control_file(gear_context):
    """
    Populate the control file with as many values as possible to reduce the burden on the user to
    modify the values.
    Args
        gear_context (GeartoolkitContext): gear info, chiefly including context.config with all the config.json values.
    Returns
        None
    """

    # Load the example control file.
    init_cntl_file = op.join(op.dirname(__file__), "data", "init.control")
    with open(init_cntl_file, "r+") as orig_cf:
        contents = [line for line in orig_cf.readlines() if line.strip()]
    contents_dict = parser.parse_init_control_file(gear_context, contents)

    # Add options from the UI
    ui_dict = parser.add_config_opts(gear_context)
    contents_dict.update(ui_dict)

    # Select the first slice in the specified range.
    contents_dict.update({"islice": main.determine_slices_to_process(gear_context)[0]})

    # Put the control file in the output folder, so that PIs can inspect the commands post-hoc
    cntl_file_path = op.join(
        gear_context.output_dir,
        "slice" + str(main.determine_slices_to_process(gear_context)[0]) + ".control",
    )
    # Create the req'd first line
    with open(cntl_file_path, "w+") as cf:
        cf.write(" title= '{}'\n".format(get_ptpt_label(gear_context)))
        # Add the populated contents array
        for k, v in contents_dict.items():
            cf.write(parser.format_values(k, v))

    # Double check that the formatted text is compliant with LCModel's strict standards
    check_cntl_file(gear_context, cntl_file_path)


# Make name safe (LCModel cannot handle spaces)
def check_cntl_file(gear_context, input_cntl_file):
    """
    LCModel has a specific format that it requires for the control file order. Check to make sure the
    keywords are at the beginning and end. Also, ensure that there is a single space
    before the first bit of text on each line, per the LCModel manual.
    Args
        cntl_file_path (file path): User-specified and created file with parameters for the analysis
    Returns
        None
    """
    cntl_file_path = op.join(
        gear_context.output_dir,
        "slice" + str(main.determine_slices_to_process(gear_context)[0]) + ".control",
    )

    with open(input_cntl_file, "r+") as cf:
        contents = [line for line in cf.readlines() if line.strip()]

    # Edit the control file for /flywheel/v0 references and config options.
    num_lines = len(contents)
    log.debug(f"Control file has {num_lines} lines to check")
    for idx, line in enumerate(contents):
        # Make sure there is leading whitespace and no illegal characters
        new_line = make_names_compliant(gear_context, line)

        if new_line:
            # Certain entries are cleared by make_names_compliant, so they
            # can be added with _add_
            contents[idx] = new_line

    # Make sure the beginning and end lines are per the LCModel specs
    first = contents[0]
    if "$LCMODL" not in first:
        contents.insert(0, " $LCMODL")
    else:
        contents[0] = f" {first.strip()}"

    last = contents[-1]
    if "$END" not in last:
        contents.insert(len(contents) + 1, " $END")
    else:
        contents[-1] = f" {last.strip()}"
    end_ix = contents.index(" $END")
    # Add LCModel key
    if "key" not in contents:
        contents.insert(end_ix, " key= 210387309")

    with open(cntl_file_path, "w+") as wf:
        for line in contents:
            wf.write(f"{line}\n")


def make_names_compliant(gear_context, txt):
    """
    Insert the leading white space and make sure there are no spaces within file names for
    the control file.
    Args
        txt (str): input from control file lines
    Returns
        new_txt (str): formatted, checked strings, compliant with LCModel requirements.
    """
    if "=" in txt:
        first, second = txt.split("=")

        if first.strip() in [
            "srcraw",
            "savdir",
            "filraw",
            "filcsv",
            "filbas",
            "filps",
            "filtab",
        ]:
            second = add_reqd_files(gear_context, first)
            new_text = f" {first.strip()} = '{second}'"
        else:
            second = second.strip("\n")
            safe_patt = re.compile(r"[^A-Za-z0-9_\+\-\'.]+")
            # Replace non-alphanumeric characters with "_"; aimed as making sure there are no
            # spaces in the value.
            second = re.sub(safe_patt, "_", second)
            # FORTRAN requires only the first letter or .TRUE/.FALSE. Correct, so MYCONT 2 does not appear.
            # Trying f, F, and .FALSE was still throwing this error, however.
            if second.lower() in ["'true'", "'false'", "false", "true"]:
                second = second.lower().strip("'")[0]
            new_text = f" {first.strip()} = {second.strip('_')}"
        return new_text


def add_reqd_files(gear_context, field):
    """
    Each of the file inputs/locations provided to LCModel via the control file
    needs to be relative to the location in Docker. If the locations were provided
    in a user-supplied control file, the entries were stripped to make sure that
    the files from the config["inputs"] are the locations referenced in the
    control file.
    Args
        gear_context (GearToolkit Context): gear info
        field (str): name of the attribute to match; the name before the "=" in each
        line of the control file.
    Returns
        modified control file
    """

    field = field.strip()
    ptpt_label = get_ptpt_label(gear_context)
    if field == "srcraw":
        val = gear_context.get_input_path("rda_file")
        # not sure this is the same as RAW. Gaurav had an rda here. May need different input field
    elif field == "savdir":
        val = f"{gear_context.output_dir}"
    elif field == "filraw":
        # This file is likely to be generated automatically based on the metabolites and
        # basis set. For now, it is an input required from the user.
        val = gear_context.get_input_path("RAW")
    elif field == "filcsv":
        val = f"{gear_context.output_dir}/{ptpt_label}_slice00.csv"
    elif field == "filbas":
        val = gear_context.get_input_path("basis_set")
    elif field == "filps":
        ps_dir = op.join(gear_context.output_dir, "ps_files")
        if not op.exists(ps_dir):
            makedirs(ps_dir)
        val = f"{ps_dir}/{ptpt_label}.ps"
    elif field == "filtab":
        table_dir = op.join(gear_context.output_dir, "table_files")
        if not op.exists(table_dir):
            makedirs(table_dir)
        val = f"{table_dir}/{ptpt_label}_table"

    return val.strip("\t")


def get_ptpt_label(gear_context):
    """
    Identify the participant label to be propagated through the analysis.
    Args
        gear_context
    Returns
        label (str): simple identifier based on the container's Subject ID
    """
    try:
        destination = gear_context.client.get(gear_context.destination["id"])
        ptpt = gear_context.client.get(destination.parents["subject"])
        return ptpt.label
    except ApiException as e:
        log.error(
            f"Could not find analysis container.\nFormal error is {e}\nGear must exit."
        )
        sys.exit(1)


def edit_slice_number(gear_context, chg_val):
    """
    Change the slice number in the control file so that the user gets more consistent feedback
    and can restart analyses, if the program goes offline halfway through. The control file
    will already exist and just needs the islice value to be updated.
    Args
        gear_context (GeartoolkitContext): gear info, chiefly including context.config with all the config.json values.
        chg_val (int): new slice number
    Returns
        updates the control file
    """
    orig_cntl_file = glob(op.join(gear_context.output_dir, "slice*.control"))[0]
    cntl_file_path = op.join(
        gear_context.output_dir,
        "slice" + str(chg_val) + ".control",
    )
    if orig_cntl_file != cntl_file_path:
        "Replicating original control file"
        shutil.copyfile(orig_cntl_file, cntl_file_path)

    with open(cntl_file_path, "r") as cf:
        contents = [line for line in cf.readlines()]

    with open(cntl_file_path, "w") as cf:
        for i, line in enumerate(contents):
            if "islice" in line:
                contents[i] = f" islice={chg_val}\n"
            if "filcsv" in line:
                # Direct the output of the model to a specific file for the slice
                name = contents[i].replace("'", "").split("=")[-1].split(".")[0]
                if int(chg_val) >= 10:
                    new = f"_slice{chg_val}"
                else:
                    new = f"_slice0{chg_val}"
                name = re.sub(r"_slice[0-9][0-9]", new, name).strip()
                contents[i] = f" filcsv= '{name.strip()}.csv'\n"  # num to str needed?
                # if not op.exists(name + ".csv"):
                #    Path(name + ".csv").touch()
                #    chmod(name + ".csv", 0o664)
        cf.writelines(contents)


def zip_it(gear_context):
    """Archive the potentially extensive list of ps and table files."""
    for d in ["ps_files", "table_files"]:
        log.info(f"Zipping {d}")
        dst = op.join(gear_context.output_dir, d)
        # Zip it up
        shutil.make_archive(dst, "zip", dst)
        # Check for extra files left in output
        name = d.split("_")[0]
        # Find the files
        files = [
            f
            for f in glob(op.join(gear_context.output_dir, "*" + name + "*"))
            if op.isfile(f) and "zip" not in f
        ]
        if files:
            # Delete the files
            for f in files:
                log.debug(f"Deleting {f}")
                remove(f)
