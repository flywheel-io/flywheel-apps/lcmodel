# lcmodel
Spectroscopy data processing with LCMODEL + Flywheel

MRS data can be processed through traditional [LCModel](http://s-provencher.com/pub/LCModel/manual/manual.pdf) parameters and set up. This gear uses Docker
and the free LCModel license to return spectra to Flywheel. Users should expect the full capability
available from the LCModel commandline options. While there are few defaults for the configuration
options, users are encouraged to reference the LCModel manual and include most available fields.

## LCMODEL Pipeline
### Inputs
- rda_file: The raw data from the scanner is used to populate the 'srcraw' field in the control file.  
- control_file (optional): specify the options for the LCModel algorithm with a control file built by
your in-house scripts. The gear will respect all non-file path fields that are present in a submitted
control file. If the options are submitted through a control file, then few options on the "configuration"
tab will need to be selected (i.e., no need to specify on the config tab, if a control file is supplied).  
- water_reference_data (optional): If a separate spectra for water was collected, furnish the file in this input.  
- RAW: In the future, this field may be auto-generated. For now, please supply the "RAW" file generated for
LCModel analysis. The data in this file represents the idealized spectra, given the metabolites collected
in light of the field strength and other acquisition parameters. This input becomes "filraw" in the control file.  


### Config options
- Bruker: Set to true, if using Bruker, Philips, or Toshiba data
- chomit: Comma-separated list of LCModel-approved abbreviations for the metabolites to omit from fitting/reporting.
- dry-run: boolean telling the gear to skip actual processing
- debug: Set the log-level
- icolst: Control file parameter for first column (to be analyzed)
- icolen: Control file parameter for final column (to be analyzed)
- irowst: Control file parameter for first row (to be analyzed)
- irowen: Control file parameter for last row (to be analyzed)
- slices_to_process: Specific slice or set of slices to analyze
- nomit: Integer for the number of metabolites to be omitted. (Must match the length of chomit)
- ppmst: Upper limit of the spectra
- ppmend: Lower limit of the spectra window
- seqacq: FOR BRUKER USERS ONLY. Set to true when the imaginary part of the complex pair is acquired one sample time after the real part.
- vendor: Currently, only supports Siemens.
- volume: voxel size; used in RAW file generation

All control files are checked for formatting and compliance with LCModel instruction.
  Specification of control files is encouraged to avoid lengthy set up times, required by filling
  fields in the configuration tab.  
  
Underscored notes for the current gear
- MRS (rda) data files are required. RAW data files may be constructed per the 
  vendor-specific, LCModel bin2raw method within the gear in the future.
  However, direct input from the user is currently required.
- Basis sets are also required input at this time.
- Water referenced data is optional, dependent on vendor and scan type.

### Outputs
- Final, formatted control file submitted to LCModel. 
- LCModel outputs:  
  + csv's with the metabolite concentrations are in the output folder/on the analysis container.
  

TODO put outputs on the session containers and make sure that the .ps options are properly specified in gear_args. 




