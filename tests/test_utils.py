"""Module to test main.py"""
import os.path as op
from unittest.mock import MagicMock, PropertyMock, patch

import pytest
import suspect

from fw_gear_lcmodel import fw_utils, set_control_file
from tests import test_main

# Grab the test directory, so that there is an output directory for the text file writes.
mod_dir = op.dirname(op.abspath(test_main.__file__))


def test_loadInitCntlFile(mock_context):
    """Test that the bare minimum control file can be populated from the sample control file and rda hdr"""
    mock_context.get_input_file.return_value = "squeaky"
    mock_context.get_input_path.return_value = "/golden/brick/road"
    mock_rda_fp = op.join(op.dirname(__file__), "data", "sample.rda")
    mock_rda = set_control_file.skim_rda_hdr(mock_rda_fp)
    with patch(
        "fw_gear_lcmodel.parser.fw_utils.get_ptpt_label", return_value="abracadabra"
    ):
        with patch(
            "fw_gear_lcmodel.parser.set_control_file.skim_rda_hdr",
            return_value=mock_rda,
        ):
            with patch("fw_gear_lcmodel.parser.suspect.io.load_rda") as mock_rda_data:
                mock_rda_data.return_value.shape = [16, 16, 16, 512]
                mock_rda_data.return_value.np = 512
                with patch(
                    "fw_gear_lcmodel.fw_utils.main.determine_slices_to_process",
                    return_value=[1],
                ):
                    fw_utils.load_init_control_file(mock_context)
    with open(
        op.join(op.dirname(__file__), "test_output", "slice1.control"), "r+"
    ) as cf:
        contents = [line for line in cf.readlines() if line.strip()]
    assert contents[0] == " $LCMODL\n"
    assert contents[-1] == " $END\n"
    assert len(contents) == 29


@patch("fw_gear_lcmodel.fw_utils.main.determine_slices_to_process", return_value=[1])
def test_check_cntl_file(mock_slices, mock_context):
    """
    Does the method check and alter the input control file to ensure formatting?
    """
    file_loc = op.join(mock_context.output_dir, "slice1.control")
    if not op.isfile(file_loc):
        test_loadInitCntlFile(mock_context)

    fw_utils.check_cntl_file(mock_context, file_loc)

    with open(file_loc, "r") as result:
        txt = result.readlines()
        first_line = txt[0]
        last_line = txt[-1]
        key = txt[-2]
    assert first_line == " $LCMODL\n"
    assert last_line == " $END\n"
    assert key == " key= 210387309\n"


def test_ptpt_label(mock_context):
    """
    Does the method parse the configuration to return a participant label for other methods
    to use for organization?
    """

    ptpt_label = fw_utils.get_ptpt_label(mock_context)
    assert ptpt_label
    assert mock_context.client.get.call_count == 2


@pytest.mark.parametrize(
    "mock_txt, expected",
    [
        ("chomit= 'h2o'", "chomit= 'h2o'"),
        ("craziness= idk*about_that_", "craziness= idk_about_that"),
        ("savdir= /discarded/path", "savdir= output"),
    ],
)
def test_make_names_compliant(mock_txt, expected, mock_context):
    """
    Are illegal chars stripped from text and reformatted to have a leading whitespace?
    """
    out = fw_utils.make_names_compliant(mock_context, mock_txt)
    assert expected.split(" ")[-1] in out


@pytest.mark.parametrize(
    "mock_field, expected",
    [
        ("srcraw", "/does/not/matter"),
        ("savdir", "output"),
        ("filraw", "/does/not/matter"),
        ("filcsv", "output/example_101_slice00.csv"),
        ("filbas", "/does/not/matter"),
    ],
)
def test_add_reqd_files(mock_field, expected, mock_context):
    """
    Does the method add lines of text to point to files?
    """
    mock_context.get_input_path.return_value = "/does/not/matter"
    with patch("fw_gear_lcmodel.fw_utils.get_ptpt_label", return_value="example_101"):
        out = fw_utils.add_reqd_files(mock_context, mock_field)
    assert expected in out
