"""Module to test main.py"""
import os.path as op
from unittest.mock import MagicMock, PropertyMock, mock_open, patch

import pytest

from fw_gear_lcmodel import fw_utils, main


def test_run_noControlFile_usesInitCntl(common_mocks, mock_context):
    """
    Run is the main method, which calls other smaller methods. Test that a control file
    will be attempted to be built, if none is provided in the configuration.
    """
    (
        mock_parse,
        mock_checkRaw,
        mock_fwUtils,
        mock_runLCM,
        mock_slices,
        mock_vs,
    ) = common_mocks
    mock_parse.return_value = (
        False,
        {"raw_data": "/fake/path"},
        {"not_a": "real_dict"},
    )
    mock_context.output_dir = op.join(op.dirname(mock_context.output_dir), "data")
    main.run(mock_context)
    mock_checkRaw.assert_called_once()
    mock_fwUtils.load_init_control_file.assert_called_once()
    assert mock_fwUtils.load_init_control_file.call_count == 1


def test_runControlFile_usesCheckCntl(common_mocks, mock_context):
    """
    When running all the smaller methods in "run", make sure user-submitted control
    files are respected and simply checked for compliance to LCModel requirements.
    """
    (
        mock_parse,
        mock_checkRaw,
        mock_fwUtils,
        mock_runLCM,
        mock_slices,
        mock_vs,
    ) = common_mocks
    mock_parse.return_value = (
        False,
        {"control_file": "/fake/path/to/example/cntl_file"},
        {"not_a": "real_dict"},
    )
    # Change the location to find the init control file
    mock_context.output_dir = op.join(op.dirname(mock_context.output_dir), "data")
    main.run(mock_context)
    mock_checkRaw.assert_called_once()
    assert mock_fwUtils.load_init_control_file.call_count == 0
    mock_fwUtils.check_cntl_file.assert_called_once()


def test_check_RAW_file_dcms(mock_context, caplog):
    """
    Raw data from the scanner should not be used as the input for this gear.
    Test a raw extension to make sure that the process exits.
    """
    mock_context.get_input_path = PropertyMock(return_value="/fake/path/to/wrong.dcm")
    caplog.set_level("DEBUG")
    with pytest.raises(SystemExit):
        main.check_RAW_file(mock_context)
    assert "Must input RAW" in caplog.text


def test_check_RAW_file_converted(mock_context, caplog):
    mock_context.get_input_path = PropertyMock(return_value="/fake/path/to/right.rda")
    caplog.set_level("DEBUG")
    main.check_RAW_file(mock_context)
    assert "file was provided as input" in caplog.text


# Parametrize and Testcase don't play well together. Move this test outside the class.
@pytest.mark.parametrize(
    "mock_stdout, expected", [(None, 1), ("Hey look, there was an answer!", 1)]
)
@patch("fw_gear_lcmodel.main.get_voxel_size", return_value=100)
@patch("fw_gear_lcmodel.main.sp.run")
def test_run_lcmodel(mock_run, mock_vs, mock_stdout, expected, mock_context):
    """
    Check error handling within the run_lcmodel method.
    The second condition must be a 1, b/c the timer() settings will return less than
    1 second.
    """
    mock_context.config = {"debug": False, "output_dir": "/nvm/im/good"}
    if mock_stdout:
        mock_run.return_value.stdout = mock_stdout.encode("utf-8")
        mock_run.return_value.stderr = ""
    mock_run.return_value.returncode = expected
    result = main.run_lcmodel(mock_context, 1)
    assert result == expected


@patch("fw_gear_lcmodel.main.get_voxel_size", return_value=100)
@patch("fw_gear_lcmodel.main.sp.run")
def test_run_lcmodel_error(mock_run, mock_vs, mock_context):
    """
    Check error handling within the run_lcmodel method.
    """
    mock_context.config = {"debug": False, "output_dir": "/nvm/im/good"}
    mock_run.side_effect = RuntimeError
    result = main.run_lcmodel(mock_context, 1)
    assert result == 1


def test_get_voxel_size_returns(mock_context):
    with open(op.join(mock_context.output_dir, "fake_rda.rda")) as data:
        rda = {}
        for line in data.readlines():
            try:
                k, v = line.split("=")
                rda[k.strip()] = v.strip()
            except Exception:
                pass

    with patch("fw_gear_lcmodel.main.set_control_file.skim_rda_hdr", return_value=rda):
        voxel_size = main.get_voxel_size(mock_context)
    assert voxel_size == 6
