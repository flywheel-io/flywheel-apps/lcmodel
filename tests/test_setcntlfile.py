import os.path as op

import pytest

from fw_gear_lcmodel import set_control_file


def test_defineDefaults_returns():
    """Returns a simple dictionary. This test will help track if entries are added,
    so that coder can determine if other methods will be needed to accommodate new entries."""
    mock_defaults = set_control_file.define_defaults()
    assert len(mock_defaults) == 16


def test_skimRdaHdr_readsFile():
    """Does skim_rda_hdr read and filter the sample rda to a dictionary of appropriate length?"""
    mock_rda_fp = op.join(op.dirname(__file__), "data", "sample.rda")
    mock_contents = set_control_file.skim_rda_hdr(mock_rda_fp)
    assert len(mock_contents) == 57
