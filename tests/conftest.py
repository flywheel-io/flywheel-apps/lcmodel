"""
Set up parameters for testing. Picked up by pytest automatically.
"""

import os.path as op
from unittest.mock import MagicMock

import pytest

import tests.test_main


@pytest.fixture
def mock_context(mocker):
    mocker.patch("flywheel_gear_toolkit.GearToolkitContext")
    mod_dir = op.dirname(op.abspath(tests.test_main.__file__))
    gtk = MagicMock(autospec=True, output_dir=op.join(mod_dir, "test_output"))
    return gtk


@pytest.fixture
def common_mocks(mocker):
    mock_vs = mocker.patch("fw_gear_lcmodel.main.get_voxel_size", return_value=100)
    mock_slices = mocker.patch(
        "fw_gear_lcmodel.main.determine_slices_to_process", return_value=[1]
    )
    mock_runLCM = mocker.patch("fw_gear_lcmodel.main.run_lcmodel")
    mock_fwUtils = mocker.patch("fw_gear_lcmodel.main.fw_utils")
    mock_checkRaw = mocker.patch("fw_gear_lcmodel.main.check_RAW_file")
    mock_parse = mocker.patch("fw_gear_lcmodel.main.parser.parse_config")

    return mock_parse, mock_checkRaw, mock_fwUtils, mock_runLCM, mock_slices, mock_vs
