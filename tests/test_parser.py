"""Module to test parser.py"""
import os.path as op
from unittest import TestCase
from unittest.mock import MagicMock, PropertyMock, patch

import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_lcmodel import parser


@patch(
    "flywheel_gear_toolkit.GearToolkitContext",
    spec=True,
    config={"debug": False},
    config_json={
        "inputs": {
            "Basis-set": "/fake/basis.basis",
            "Control_file": "/fake/control.file",
        }
    },
)
class TestParse(TestCase):
    def test_parses_args_for_translation(self, mock_context):
        """
        Parser should handle debug field, which does not have a translation
        key in the manifest. Main purpose of the test is to show that the
        exception clause is accessible.
        """
        debug, gear_args, image_filepaths = parser.parse_config(mock_context)
        assert debug == False
        assert gear_args is not []
        assert image_filepaths is not []

    def test_parses_gear_conditions(self, mock_context):
        """
        debug is a config option for the Gear, not LCModel. It should not trigger adding any values
        to the gear_args. Future: This test can include any other kwargs that should be added to "do_not_include"
        """
        mock_context.config = {"debug": True}
        args = parser.parse_commands(mock_context)
        assert len(args) == 0

    def test_parser_checks_metabolite_number_correct(self, mock_context):
        """
        The number of metabolites and list of metabolites to be omitted must match.
        Test the case where number and length of list DO match + another config option was set.
        """
        mock_context = PropertyMock(
            config={"nomit": 2, "chomit": ["Met", "Gly"], "fake_property": "string"}
        )
        with self.assertRaisesRegex(
            self.failureException,
            r"^no logs of level INFO or higher triggered on root$",
        ):  # assert no errors or variable resetting
            with self.assertLogs() as log:
                args = parser.parse_commands(mock_context)
        assert len(args) == 3

    def test_parser_checks_metabolite_number_incorrect(self, mock_context):
        """
        The number of metabolites and list of metabolites to be omitted must match. Here, the
        listed metabolites are greater than the number specified. Should enter the condition
        to correct the nomit field.
        """
        mock_context = PropertyMock(
            config={"nomit": 2, "chomit": ["Met", "Gly", "Ala"]}
        )
        with self.assertLogs() as log:
            args = parser.parse_commands(mock_context)
        assert len(args) == 2
        assert (
            mock_context.config["nomit"] == 3
        )  # The value should have been updated in the method
        assert log.records[-1].levelname.lower() == "info"

    def test_parser_exits_no_metabolites(self, mock_context):
        """
        The number of metabolites and list of metabolites to be omitted must match.
        Test that a missing list of metabolites produces a (fatal) error.
        """
        mock_context = PropertyMock(config={"nomit": 2})
        with self.assertLogs() as log:
            with pytest.raises(SystemExit):
                parser.parse_commands(mock_context)
        assert "no metabolites are identified" in log.output[0]

    def test_parser_calls_files(self, mock_context):
        """
        Test the image files are to be located.
        """
        with patch(
            "flywheel_gear_toolkit.GearToolkitContext.get_input_path"
        ) as mock_get_input:
            parser.gather_input_files(mock_context)
        assert (
            mock_get_input.call_count == 2
        )  # Number of inputs in config_json that are not related to the key
